'use strict';

var converters = require('../converters'),
    chai = require('chai'),
    sinonChai = require('sinon-chai'),
    moment = require('moment'),
    should;

chai.use(sinonChai);
should = chai.should();   

describe('converters', function(){
    it ('#identity() returns exactly what was given', function(){
        converters.identity('a').should.equal('a');
    });
    
    it('#date() converts date strings to dates', function(){
        converters.date('2016-07-01').should.eql(new Date('2016-07-01'));
    });
    
    it('#datePlusTime() adds hours and minutes to a date', function(){
        var startDate = moment('2016-07-01').toDate();
        converters.datePlusTime(startDate, '23:33').should.eql(new Date('2016-07-01T23:33'));
        converters.datePlusTime(startDate, '23:33 PM').should.eql(new Date('2016-07-01T23:33'));

    });
    
    it('#currency() extracts a number from a string', function(){
       converters.currency('3 seats at:                                                    $126').should.equal(126);
       should.not.exist(converters.currency('Sold out'));
       converters.currency('$39').should.equal(39);
    });
});