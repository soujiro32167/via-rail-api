'use strict';

var log = require('log');

module.exports = {
    identity: function(a){ return a },
    date: function(dateString){ return new Date(dateString) },
    datePlusTime: function(startDate, timeString){
        var result = new Date(startDate),
            match = /(\d{2}):(\d{2})\s*(?:AM|PM)?/.exec(timeString);
        result.setHours(match[1]);
        result.setMinutes(match[2]);
        result.setSeconds(0);
        result.setMilliseconds(0);
        
        return result;
    },
    currency: function(currencyString) {
        var match = currencyString.match(/\$(\d+)/);
        return match && parseInt(match[1]);
    }
};