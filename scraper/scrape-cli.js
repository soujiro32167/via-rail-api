const Log = require('log'),
  moment = require('moment'),
  scrape = require('./scrape').scrape,
  dbAdapter = require('./db-adapter'),
  argv = require('yargs')
    .options({
      'from': {
        'description': 'Source of the journey',
        'demand': true,
        'alias': 'f'
      },
      'to': {
        'description': 'Destination of the journey',
        'demand': true,
        'alias': 't'
      },
      'date': {
        'description': 'Date of the journey',
        'coerce': s => moment(s).toDate(),
        'default': new Date()
      },
      'duration': {
        'description': 'A range of dates to scrape, starting today. e.g. "3-months"',
        'coerce': d => {
          let [number, unit] = d.split('-');
          number = +number;
          return moment.duration(number, unit);
        },
        'default': '1-days',
        'alias': 'd'
      },
      'persist': {
        'description': 'Save results to database',
        'boolean': true,
        'default': false,
        'alias': 'p'
      },
      'log-level': {
        'description': 'Amount of diagnostic feedback',
        'alias': 'v',
        'choices': ['error','info', 'debug'],
        'default': process.env.LOG_LEVEL || 'info'
      }
    })
    .help('h')
    .example('$0 MTRL TRTO', 'Get all trains from Montreal to Toronto for today')
    .example('$0 -f TRTO -t MTRL --date 2017-10-1', 'Get all trains from Toronto to Montreal for October first, 2017')
    .example('$0 -f TRTO -t MTRL --duration=3-months', 'Get all trains from Toronto to Montreal for 3 months, starting today')
    .argv,
    log = new Log(argv.logLevel),
    nop = () => null;
    
log.debug('argv', {...argv, duration: argv.duration.humanize(), d: argv.duration.humanize()});

// run the scraper

(async function(){
  // scrape for a date range
  const endDate = moment(argv.date).clone().add(argv.duration);
  let results = [];
  log.debug('date [%s] endDate [%s]', argv.date, endDate);
  for (let scrapeDay = moment(argv.date); scrapeDay.isBefore(endDate); scrapeDay.add(1, 'days')){
    log.info('scraping for day', scrapeDay);
    results.push(scrape(argv.from, argv.to, scrapeDay.toDate(), {logLevel: argv.logLevel}))
  }
  try {
    // once all the results are in...
    const scheduleArrays = await Promise.all(results);
    // flatten results into a single array
    const schedules = scheduleArrays.reduce((acc, cur) => acc.concat(cur), []);
    // dump results
    console.log(JSON.stringify(schedules));
    if (argv.persist){
      await dbAdapter.saveToDb(schedules)
    }
  } catch (errors){
    log.error(errors);
  }  
})()  
