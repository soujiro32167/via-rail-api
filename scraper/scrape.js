'use strict';

const path = require('path'),
  fs = require('fs'),
  request = require('request'),
  cheerio = require('cheerio'),
  Log = require('log'),
  converters = require('./converters');
  
/**
 * A function that uses context to transform text column values to meaningfull values
 * @param {object} context
 * @param {Date} context.date the departure date
 * @return {array} an array of converters with column names and conversion functions
 */
function columnMapping (context){
  return [
    {
      //name: 'trainNumber'
      // train number not currently in use
    },
    {
      name: 'departureTime',
      // for convenience, attach a full date to the deprature time
      converter: converters.datePlusTime.bind(undefined, context.date)
    },
    {
      name: 'arrivalTime',
      // for convenience, attach a full date to the arrival time
      converter: converters.datePlusTime.bind(undefined, context.date)
    },
    {
      // name: 'duraion'
      // duration not in use
    },
    {
      name: 'specialFare',
      converter: converters.currency
    },
    {
      name: 'economyDiscountedFare',
      converter: converters.currency
    },
    {
      name: 'economyRegularFare',
      converter: converters.currency
    },
    {
      name: 'businessDiscountedFare',
      converter: converters.currency
    },
    {
      name: 'businessRegularFare',
      converter: converters.currency
    }];
};

const fareColumns = ['specialFare', 'economyDiscountedFare', 'economyRegularFare', 'businessDiscountedFare', 'businessRegularFare'];
const logDir = path.resolve(__dirname, 'logs');
const sampleDir = path.resolve(__dirname, 'samples');

//var MongoClient = require('mongodb').MongoClient

var scrape = function (from, to, date, options) {
  options = Object.assign({}, options, {
    adults: '1',
    seniors: '0',
    youths: '0',
    children: '0',
    infants: '0',
    withTaxes: 'on',
    discountCode: ''
  });
  var log = new Log(options.logLevel),
    url = 'https://reservia.viarail.ca/search/setSearch.aspx?l=en',
    viaPostParams = {
      cmbStationsFrom_value: from,
      cmbStationsTo_value: to,
      cmbMonthsFrom: (date.getMonth() + 1) + '',
      cmbDaysFrom: (date.getDate()) + '',
      cmbNbAdults: options.adults,
      cmbNbSeniors: options.seniors,
      cmbNbYouths: options.youths,
      cmbNbChilds: options.children,
      cmbNbInfants: options.infants,
      withtaxes: options.withTaxes,
      txtDiscountCode: options.discountCode,
      // bullshit
      txtFareWithTaxes: 'TRUE',
      rad_trip: 'oneway',
      cmbScheduleTo: '0',
      cmbScheduleFrom: '0',
      cmbMonthsTo: '',
      cmbDaysTo: ''
    },
    promise,
    cookieJar = request.jar(),
    columnMapper = columnMapping({'date': date});
    
  log.info('scraping from [%s] to [%s] on [%s]', from, to, date);
  
  promise = new Promise((resolve, reject) => {
    request.post({
      url: url, 
      form: viaPostParams, 
      followAllRedirects: true,
      jar: cookieJar
    }, (error, response, html) => {
      if (error){
        //console.error('Error', response);
        reject(error);
        return promise;
      }
      //fs.writeFile(`${sampleDir}/sample_response.html`, html, 'UTF-8');
      log.debug('cookies so far:', cookieJar);
  
      
      var $ = cheerio.load(html);
      // round 1: differentiate column-schedule-info

      var scraped = $('.train-route-container')
        // for each container, extract columns
        .map(function(){
          var columnObject =  $(this).find('.column')
          // slice out the '+' toggle
          .slice(1)
          // for each column, map column title to value
          .map(function(index){
            var result, mappingObject, rawValue;
            // adjust index to 0-base
            //index--;
            mappingObject = columnMapper[index];
            rawValue = $(this).text().trim();
            
            if (mappingObject.name){
              // a recognized column - assign parameters to the conversion function
              log.debug('converting index %s to column %o', index, mappingObject);
              result = Object.assign({}, mappingObject, { 
                value: rawValue  
              });
              log.debug('with raw value %s', rawValue)
            }
            return result;
          }).get() // end of jQuery, array begins
          // filter out empty objects
          .filter(converters.identity)
          // apply the converter, mapping the result to a column name
          .reduce(function(accumulator, current){
              accumulator[current.name] = current.converter(current.value);
              return accumulator;
          }, {});
          
          // add metadata
          columnObject.from = from;
          columnObject.to = to;
          columnObject.collectedOn = new Date();
          columnObject.cheapestFare = Math.min.apply(null,
            fareColumns
              .map(fareColumn => columnObject[fareColumn])
              .filter(converters.identity)
            );
          
          return columnObject;
        }).get();
      
      resolve(scraped);
    });
  });
  
  return promise;
  
};

module.exports = {
  scrape: scrape
}