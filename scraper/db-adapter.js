const MongoClient = require('mongodb').MongoClient;
const Log = require('Log');
const options = {
    mongoConnectParams: [
        process.env.MONGO_CONNECT_URL || 'mongodb://localhost:27017/via',
        process.env.MONGO_PASSWORD ? {
            auth: {
                user: process.env.MONGO_USER,
                password: process.env.MONGO_PASSWORD
            }
        } : null
    ]
    // remove nulls
    .filter(p => !!p),
    mongoDbName: process.env.MONGO_DB_NAME || 'via',
    logLevel: process.env.LOG_LEVEL
};

const log = new Log(options.logLevel);


async function saveToDb(schedulesForDate) {
    log.debug('options:', options);
    log.info('connecting to db...');
    let client;
    try {
        client = await MongoClient.connect(...options.mongoConnectParams);
        log.info('Connected. inserting into db...');
        const db = client.db(options.mongoDbName);
        const result = await db.collection('schedules').insertMany(schedulesForDate);
        log.info('Schedules saved');
    } catch (error) {
        log.error('Failed to write to db', error);
    }

    if (client) {
        client.close();
    }
};

module.exports = {
    saveToDb
}
