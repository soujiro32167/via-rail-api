# VIA Rail API

A library of tools to easily search the VIA Rail train schedule

## Scraper

The scraper requests train schedules per day and optionally saves them to a database (currently MongoDB)

Example usge:
```
cd scraper && node scraper-cli.js -h
Options:
  --from, -f       Source of the journey                              [required]
  --to, -t         Destination of the journey                         [required]
  --date           Date of the journey     [default: "2018-04-05T22:51:59.684Z"]
  --duration, -d   A range of dates to scrape, starting today. e.g. "3-months"
                                                             [default: "1-days"]
  --persist, -p    Save results to database           [boolean] [default: false]
  --log-level, -v  Amount of diagnostic feedback
                           [choices: "error", "info", "debug"] [default: "info"]
  -h               Show help                                           [boolean]

Examples:
  scrape-cli.js MTRL TRTO                   Get all trains from Montreal to
                                            Toronto for today
  scrape-cli.js -f TRTO -t MTRL --date      Get all trains from Toronto to
  2017-10-1                                 Montreal for October first, 2017
  scrape-cli.js -f TRTO -t MTRL             Get all trains from Toronto to
  --duration=3-months                       Montreal for 3 months, starting
                                            today
```

## Insights

*Note: Based on train schedules from April 5th 2018 to July 5th 2018 (3 months) between Montreal and Toronto*

Here is a schema analysis using Compass for MongoDB:

![Price histogram](https://cl.ly/3R1i0A3D1y0A/Image%202018-04-05%20at%207.01.30%20PM.png)

We can see that 35% of the tickets cost 50$. But where to find them?

![Date distribution](https://cl.ly/2W1Y1j3x1X3D/Image%202018-04-05%20at%207.14.29%20PM.png)

Not a big surprise, but now we can confirm that the vast majority of cheap trains depart mid-week or Saturday. With a bit more digging, we can find the rare cheap weekend tickets...