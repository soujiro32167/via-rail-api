'use strict';

var findDeals = require('../find-deals.js'),
    chai = require('chai'),
    sinonChai = require('sinon-chai'),
    should;

chai.use(sinonChai);
should = chai.should();

var day = {
   SUNDAY: 0,
   MONDAY: 1,
   TUESDAY: 2,
   WEDNESDAY: 3,
   THURSDAY: 4,
   FRIDAY: 5,
   SATURDAY: 6
}

xdescribe('Deal Finder', function(){
    var sampleData = [{
        from: 'MTL',
        to: 'TOR',
        depart: new Date('2017-03-10 06:00')
    },
    {
        from: 'TOR',
        to: 'MTL',
        depart: new Date('2017-03-12 12:00')
    }];
    
    xit('Finds deals around a weekend', function(){
        var [there, back] = findDeals.findDeals(sampleData, {from: 'MTL', to: 'TOR'});
        there.depart.getDay().should.eql(day.FRIDAY);
        back.depart.getDay().should.eql(day.SUNDAY);
    });
});

