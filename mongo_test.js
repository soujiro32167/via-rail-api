'use strict';

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');
    
//var url = `mongodb://${process.env.IP}:27017/via`;
const url = process.env.MONGO_CONNECT_URL;

var connectionTest = function(){
    MongoClient.connect(url, function(err, db){
       assert.equal(null, err);
       console.log('connected to server');
       
       db.close();
    });
};

var insertDocuments = function(db, callback) {
    var collection = db.collection('documents');
    collection.insertMany([
        {a : 1}, {a : 2}, {a : 3}
    ], function(err, result){
        assert.equal(err, null);
        assert.equal(3, result.result.n);
        assert.equal(3, result.ops.length);
        console.log('inserted 3 documents into collection');
        callback(result);
    });
}

var updateDocument = function(db, callback){
    var collection = db.collection('documents');
    collection.updateOne({a : 2},
        { $set: {b: 1} }, function(err, result){
        assert.equal(err, null);
        assert.equal(1, result.result.n);
        console.log('updated document where a : 2');
        callback(result);
    });
};

var deleteDocument = function(db, callback) {
  // Get the documents collection
  var collection = db.collection('documents');
  // Insert some documents
  collection.deleteOne({ a : 3 }, function(err, result) {
    assert.equal(err, null);
    assert.equal(1, result.result.n);
    console.log("Removed the document with the field a equal to 3");
    callback(result);
  });
};

var findDocuments = function(db, callback) {
  // Get the documents collection
  var collection = db.collection('documents');
  // Find some documents
  collection.find({}).toArray(function(err, docs) {
    assert.equal(err, null);
    assert.equal(2, docs.length);
    console.log("Found the following records");
    console.dir(docs);
    callback(docs);
  });
};


//connectionTest();

/*MongoClient.connect(url, function(err, db){
   assert.equal(err, null);
   
   insertDocuments(db, function(result){
       db.close();
   });
});*/

MongoClient.connect(url, (err, db) => {
   assert.equal(err, null);
   
   db.collection('documents').drop();
   
   insertDocuments(db, () => {
       updateDocument(db, () => {
           deleteDocument(db, () => {
               findDocuments(db, () => {
                db.close();
               })
           })
       })
   });
});
