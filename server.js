'use strict';

var express = require('express'),
  path = require('path'),
  app = express();

var staticDir = path.resolve(__dirname, 'public');

app.use('/public', express.static(staticDir));

app.use('/logs', express.static(path.resolve(__dirname, 'logs')));

app.listen(process.env.PORT || 3000);

exports = module.exports = app;